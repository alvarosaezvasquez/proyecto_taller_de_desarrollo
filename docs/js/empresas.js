$(document).ready(function(){

    $("form").submit(function(e){
        e.preventDefault();
    });

     //REGISTRO NUEVA EMPRESA
    $("#confirmar-registro").click(function(){
        const url = "./registrarEmpresa.php";
        $.ajax({
            url: url,
            type: "POST",
            data: $("#register-form").serialize(),
            success: function (response) {
                $('#modalNuevo').modal('hide');
                const mensaje = JSON.parse(JSON.stringify(response))
                if(mensaje.conteo_err==0){
                    document.getElementById("mensajeServidor").innerHTML = mensaje.mensaje;
                    document.getElementById("alerta").className = "modal-body alert alert-success";
                    $("#Respuesta").unbind();
                    $("#Respuesta").modal("show")
                    $("#Respuesta").on('hidden.bs.modal',function(){ 
                        location.reload();
				    });
                } else {
                    errores_input(mensaje.errores, "#modalNuevo");
                }
            }
        });
    });

    //MODIFICACION DE EMPRESA
    $("#confirmar-modificacion").click(function(){
        const url = "./ModificarEmpresa.php";
        $.ajax({
            url: url,
            type: "POST",
            data: $("#modify-form").serialize(),
            success: function (response) {
                $('#modalEdicion').modal('hide');
                const mensaje = JSON.parse(JSON.stringify(response));
                if(mensaje.conteo_err==0){
                    document.getElementById("mensajeServidor").innerHTML = mensaje.mensaje;
                    document.getElementById("alerta").className = "modal-body alert alert-success";
                    $("#Respuesta").unbind();
                    $("#Respuesta").modal("show")
                    $("#Respuesta").on('hidden.bs.modal',function(){ 
                        location.reload();
				    });
                } else {
                    errores_input(mensaje.errores, "#modalEdicion");
                }
                
            }
        });
    });

    //Capturar id de empresa en el <tr>
    $(document).on("click", "#btnMod", function(){
        let element = $(this)[0].parentElement.parentElement;
        let id = $(element).attr('id-empresas');
        cargarEmpresa(id);
    });

    $(document).on("click", "#btn-cancel", function(){
        document.getElementById("register-form").reset();
    })

    $(document).on("click", "#btnReg", function(){
        document.getElementById("register-form").reset();
    })

    document.body.addEventListener("keydown", function (event) {
        if (event.code === 27 || event.code === 'Escape') {
            limpiarFormNuevo();
            limpiarFormNuevoEdicion();
        }
    });

});

function cargarEmpresa(valor) {
    $.ajax({
        url: './cargarEmpresa.php',
        type: "POST",
        data: {id:valor},
        success: function(res) {
            const empresa = JSON.parse(JSON.stringify(res));
            document.getElementById("idMod").value = empresa[0].id_empresa;
            document.getElementById("nombreEmpresaMod").value = empresa[0].nombre_empresa;
            document.getElementById("rutEmpresaMod").value = empresa[0].rut_empresa;
            document.getElementById("dirEmpresaMod").value = empresa[0].direccion_empresa;
            document.getElementById("emailEmpresaMod").value = empresa[0].correo_empresa;
            document.getElementById("telefonoEmpresaMod").value = empresa[0].telefono_empresa;
            document.getElementById("webEmpresaMod").value = empresa[0].sitioweb_empresa;
            document.getElementById("faltasMod").value = empresa[0].faltas;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
        }
    })
}

//Funcion validar RUT
function checkRut(rut) {
    // Despejar Puntos
    var valor = rut.value.replace('.', '');
    // Despejar Guión
    valor = valor.replace('-', '');

    // Aislar Cuerpo y Dígito Verificador
    cuerpo = valor.slice(0, -1);
    dv = valor.slice(-1).toUpperCase();

    // Formatear RUN
    rut.value = cuerpo + '-' + dv

    // Si no cumple con el mínimo ej. (n.nnn.nnn)
    if (cuerpo.length < 7) { 
        //rut.setCustomValidity("RUT Incompleto"); 
        return false; 
    }

    // Calcular Dígito Verificador
    suma = 0;
    multiplo = 2;

    // Para cada dígito del Cuerpo
    for (i = 1; i <= cuerpo.length; i++) {

    // Obtener su Producto con el Múltiplo Correspondiente
    index = multiplo * valor.charAt(cuerpo.length - i);

    // Sumar al Contador General
    suma = suma + index;

    // Consolidar Múltiplo dentro del rango [2,7]
    if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }

    }
    // Calcular Dígito Verificador en base al Módulo 11
    dvEsperado = 11 - (suma % 11);

    // Casos Especiales (0 y K)
    dv = (dv == 'K') ? 10 : dv;
    dv = (dv == 0) ? 11 : dv;

    // Validar que el Cuerpo coincide con su Dígito Verificador
    if (dvEsperado != dv) { 
        //rut.setCustomValidity("RUT Inválido"); 
        return false; 
    }
    // Si todo sale bien, eliminar errores (decretar que es válido)
    rut.setCustomValidity('');
}

function validarEmail(mail) {
    if ( /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i.test(mail.value)){
        mail.setCustomValidity('');
    } else {
        //mail.setCustomValidity("Email incorrecto");
        return false;
    }
}

function validarUrl(url) {
    if(url.value == ''){
        url.setCustomValidity('');
    } else {
        if(/^(ftp|http|https):\/\/[^ "]+$/.test(url.value)){
            url.setCustomValidity('');
        } else {
            //url.setCustomValidity("URL incorrecta");
            return false;
        }
    }
}

function errores_input(err, form){
    let mensajeFinal = "";

    const nombre_empresa = document.getElementById("nombreEmpresa");
    const rut_empresa = document.getElementById("rutEmpresa");
    const direccion_empresa = document.getElementById("dirEmpresa");
    const email_empresa = document.getElementById("emailEmpresa");
    const telefono_empresa = document.getElementById("telefonoEmpresa");
    const sitioweb_empresa = document.getElementById("webEmpresa");

    const nombre_empresaMod = document.getElementById("nombreEmpresaMod");
    const rut_empresaMod = document.getElementById("rutEmpresaMod");
    const direccion_empresaMod = document.getElementById("dirEmpresaMod");
    const email_empresaMod = document.getElementById("emailEmpresaMod");
    const telefono_empresaMod = document.getElementById("telefonoEmpresaMod");
    const sitioweb_empresaMod = document.getElementById("webEmpresaMod");
    
    if(err[0]==1){
        mensajeFinal+="<p>Error en el campo Nombre de la empresa</p>"
        nombre_empresa.className = "form-control is-invalid"
        nombre_empresaMod.className = "form-control is-invalid"
    } else {
        nombre_empresa.className = "form-control is-valid"
        nombre_empresaMod.className = "form-control is-valid"
    }
    if(err[1]==1){
        mensajeFinal+="<p>Error en el campo Rut de la empresa, ingrese un rut válido</p>"
        rut_empresa.className = "form-control is-invalid"
        rut_empresaMod.className = "form-control is-valid"
    } else {
        rut_empresa.className = "form-control is-valid"
        rut_empresaMod.className = "form-control is-valid"
    }
    if(err[2]==1){
        mensajeFinal+="<p>Error en el campo Dirección</p>"
        direccion_empresa.className = "form-control is-invalid"
        direccion_empresaMod.className = "form-control is-invalid"
    } else {
        direccion_empresa.className = "form-control is-valid"
        direccion_empresaMod.className = "form-control is-valid"
    }
    if(err[3]==1){
        mensajeFinal+="<p>Error en el campo E-mail de la empresa, ingrese un e-mail válido</p>"
        email_empresa.className = "form-control is-invalid"
        email_empresaMod.className = "form-control is-invalid"
    } else {
        email_empresa.className = "form-control is-valid"
        email_empresaMod.className = "form-control is-valid"
    }
    if(err[4]==1){
        mensajeFinal+="<p>Error en el campo Teléfono</p>"
        telefono_empresa.className = "form-control is-invalid"
        telefono_empresaMod.className = "form-control is-invalid"
    } else {
        telefono_empresa.className = "form-control is-valid"
        telefono_empresa.className = "form-control is-valid"
    }
    if(err[5]==1){
        mensajeFinal+="<p>Error en el campo Sitio Web, ingrese una dirección web válida</p>"
        sitioweb_empresa.className = "form-control is-invalid"
        sitioweb_empresaMod.className = "form-control is-invalid"
    } else {
        sitioweb_empresa.className = "form-control is-valid"
        sitioweb_empresaMod.className = "form-control is-valid"
    }
    if(err[6]==1){
        mensajeFinal+="<p>Error en el campo Teléfono, ingrese una dirección web válida</p>"
        telefono_empresa.className = "form-control is-invalid"
        telefono_empresaMod.className = "form-control is-invalid"
    } else {
        telefono_empresa.className = "form-control is-valid"
        telefono_empresaMod.className = "form-control is-valid"
    }
    if(err[7]==1){
        mensajeFinal+="<p>El Rut ingresado ya existe, ingrese uno diferente</p>"
        rut_empresa.className = "form-control is-invalid"
        rut_empresaMod.className = "form-control is-invalid"
    } else {
        rut_empresa.className = "form-control is-valid"
        rut_empresaMod.className = "form-control is-valid"
    }
    if(err[8]==1){
        mensajeFinal+="<p>El correo electrónico ingresado ya existe, ingrese uno diferente</p>"
        email_empresa.className = "form-control is-invalid"
        email_empresaMod.className = "form-control is-invalid"
    } else {
        email_empresa.className = "form-control is-valid"
        email_empresaMod.className = "form-control is-valid"
    }

    $(form).modal("hide");
    document.getElementById("mensajeServidor").innerHTML = mensajeFinal;
    document.getElementById("alerta").className = "modal-body alert alert-danger";
    $("#Respuesta").unbind();
    $("#Respuesta").modal("show");
    $("#Respuesta").on('hidden.bs.modal',function(){ 
        $(form).modal("show");
    });

}

function limpiarFormNuevo(){

    let nombreEmpresa = document.getElementById("nombreEmpresa");
    let rutEmpresa = document.getElementById("rutEmpresa");
    let dirEmpresa = document.getElementById("dirEmpresa");
    let emailEmpresa = document.getElementById("emailEmpresa");
    let telefonoEmpresa = document.getElementById("telefonoEmpresa");
    let webEmpresa = document.getElementById("webEmpresa");

    nombreEmpresa.className = "form-control modal-input";
    rutEmpresa.className = "form-control modal-input";
    dirEmpresa.className = "form-control modal-input";
    emailEmpresa.className = "form-control modal-input";
    telefonoEmpresa.className = "form-control modal-input";
    webEmpresa.className = "form-control modal-input";

    nombreEmpresa.value = "";
    rutEmpresa.value = "";
    dirEmpresa.value = "";
    emailEmpresa.value = "";
    telefonoEmpresa.value = "";
    webEmpresa.value = "";

}

function limpiarFormEdicion(){

    let nombreEmpresa = document.getElementById("nombreEmpresaMod");
    let rutEmpresa = document.getElementById("rutEmpresaMod");
    let dirEmpresa = document.getElementById("dirEmpresaMod");
    let emailEmpresa = document.getElementById("emailEmpresaMod");
    let telefonoEmpresa = document.getElementById("telefonoEmpresaMod");
    let webEmpresa = document.getElementById("webEmpresaMod");
    let faltas = document.getElementById("faltasMod");

    nombreEmpresa.className = "form-control modal-input";
    rutEmpresa.className = "form-control modal-input";
    dirEmpresa.className = "form-control modal-input";
    emailEmpresa.className = "form-control modal-input";
    telefonoEmpresa.className = "form-control modal-input";
    webEmpresa.className = "form-control modal-input";
    faltas.className = "form-control modal-input";

    nombreEmpresa.value = "";
    rutEmpresa.value = "";
    dirEmpresa.value = "";
    emailEmpresa.value = "";
    telefonoEmpresa.value = "";
    webEmpresa.value = "";
    faltasMod.value = "";
}