$(document).ready(function(){
	$("form").submit(function(e) {
		e.preventDefault();
	});

	//Envia por POST los input y archivo pdf para crear contrato
	$("#confirmarReg").click(async function(){
		if (!verificarCampos()){
			document.getElementById("alertaR").className = "modal-body alert alert-danger";
			document.getElementById("mensajeRespuesta").innerHTML = "<p>Por favor, rellene todos los campos del formulario</p>";
			$("#ModalReg").modal("hide");
			$("#Respuesta").unbind();
			$("#Respuesta").modal("show");
			$("#Respuesta").on('hidden.bs.modal',function(){ 
				$("#ModalReg").modal("show");
			});
			return;
		}

		let formData = new FormData();
		var list = document.getElementById("form-Reg").getElementsByClassName("modal-input");
		if (list && list.length>0){
			for (let i=0;i<list.length;i++){
				formData.append(list[i].id,list[i].value);
			}
		}
		formData.append("selEmpresa", document.getElementById("selEmpresa").value);
		formData.append("archContrato", document.getElementById("archContrato").files[0]);

		const url = './registrarContrato.php';
		const promise = await fetch(url,{
			method: 'POST',
			mode: "no-cors",
			body: formData
		});

		//Revisa la respuesta
		if(promise.ok) {
			const resp = await promise.json();
			if (resp.done) {
				document.getElementById("alertaR").className = "modal-body alert alert-success";
				document.getElementById("mensajeRespuesta").innerHTML = resp.mensaje;
			} else {
				document.getElementById("alertaR").className = "modal-body alert alert-danger";
				if (resp.nerr==0){
					document.getElementById("mensajeRespuesta").innerHTML = resp.mensaje;
				} else {
					document.getElementById("mensajeRespuesta").innerHTML = mnsError(resp.errores,'');
				}
			}
			$("#ModalReg").modal("hide");
			$("#Respuesta").unbind();
			$("#Respuesta").modal("show");
			if (resp.done) {
				$("#Respuesta").on('hidden.bs.modal',function(){ 
					location.reload();
				});
			} else {
				$("#Respuesta").on('hidden.bs.modal',function(){ 
					$("#ModalReg").modal("show");
				});
			}
		}
	});

	$("#confirmarMod").click(async function(){
		if (!verificarCamposM()){
			document.getElementById("alertaR").className = "modal-body alert alert-danger";
			document.getElementById("mensajeRespuesta").innerHTML = "<p>Por favor, rellene todos los campos del formulario</p>";
			$("#ModalMod").modal("hide");
			$("#Respuesta").unbind();
			$("#Respuesta").modal("show");
			$("#Respuesta").on('hidden.bs.modal',function(){ 
				$("#ModalMod").modal("show");
			});
			return;
		}

		let formData = new FormData();
		var list = document.getElementById("form-Mod").getElementsByClassName("modal-input");
		if (list && list.length>0){
			for (let i=0;i<list.length;i++){
			formData.append(list[i].id,list[i].value);
			}
		}
		formData.append("Mid", document.getElementById("Mid").value);
		if (document.getElementById("MarchContrato").files[0]){
			formData.append("MarchContrato", document.getElementById("MarchContrato").files[0]);
		}

		const url = './modificarContrato.php';
		const promise = await fetch(url,{
			method: 'POST',
			mode: "no-cors",
			body: formData
		});

		//Revisa la respuesta
		if(promise.ok) {
			const resp = await promise.json();
			if (resp.done) {
				document.getElementById("alertaR").className = "modal-body alert alert-success";
				document.getElementById("mensajeRespuesta").innerHTML = resp.mensaje;
			} else {
				document.getElementById("alertaR").className = "modal-body alert alert-danger";
				if (resp.nerr==0){
					document.getElementById("mensajeRespuesta").innerHTML = resp.mensaje;
				} else {
					document.getElementById("mensajeRespuesta").innerHTML = mnsError(resp.errores,'M');
				}
			}
			$("#ModalMod").modal("hide");
			$("#Respuesta").unbind();
			$("#Respuesta").modal("show");
			if (resp.done) {
				$("#Respuesta").on('hidden.bs.modal',function(){ 
					location.reload();
				});
			} else {
				$("#Respuesta").on('hidden.bs.modal',function(){ 
					$("#ModalMod").modal("show");
				});
			}
		}
	});

    document.body.addEventListener("keydown", function (event) {
        if (event.code === 27 || event.code === 'Escape') {
            limpiarForm();
        }
    });
});

//Envia por POST (AJAX) una id de producto para obtener su informacion desde la bd
function cargarContrato(valor){
	$.ajax({
		url: './cargarContrato.php',
		type: "POST",
		data: {id:valor},
		success: function(res) {
			const contrato = JSON.parse(JSON.stringify(res));
			console.log(res);
			console.log(contrato);
			document.getElementById("Mid").value = contrato.id_contrato;
			document.getElementById("MselEmpresa").value = contrato.id_empresa;
			document.getElementById("MiniFecha").value = contrato.fechainicio_contrato;
			document.getElementById("MfinFecha").value = contrato.fechatermino_contrato;
			document.getElementById("Mestado").value = contrato.estado;
			document.getElementById("McantContrato").value = contrato.cant_equipos;
		},
	});
}

function mnsError(errores,prefijo){
	let mensajeFinal = " ";

	const selEmpresa = document.getElementById(prefijo+"selEmpresa");
    const cantContrato = document.getElementById(prefijo+"cantContrato");
    const iniFecha = document.getElementById(prefijo+"iniFecha");
    const finFecha = document.getElementById(prefijo+"finFecha");
    const archContrato = document.getElementById(prefijo+"archContrato");

	if(errores[0]==true){
        mensajeFinal+="<p>Error en el campo de empresa oferente</p>";
        selEmpresa.className = "form-control modal-input is-invalid";
    } else {
        selEmpresa.className = "form-control modal-input is-valid";
    }
	if(errores[1]==true){
        mensajeFinal+="<p>Error en el campo de cantidad de equipos</p>";
        cantContrato.className = "form-control modal-input is-invalid";
    } else {
        cantContrato.className = "form-control modal-input is-valid";
    }
	if(errores[2]==true){
        mensajeFinal+="<p>Error en el campo de fechas</p>";
        iniFecha.className = "form-control modal-input is-invalid";
		finFecha.className = "form-control modal-input is-invalid";
    } else {
        iniFecha.className = "form-control modal-input is-valid";
		finFecha.className = "form-control modal-input is-valid";
    }
	if(errores[3]==true){
        mensajeFinal+="<p>Error de archivo adjunto</p>";
        archContrato.className = "form-control modal-input is-invalid";
    } else {
        archContrato.className = "form-control modal-input is-valid";
    }
	return mensajeFinal;
}

function limpiarForm(){
	let selEmpresa = document.getElementById("selEmpresa");
    let cantContrato = document.getElementById("cantContrato");
    let iniFecha = document.getElementById("iniFecha");
    let finFecha = document.getElementById("finFecha");
    let archContrato = document.getElementById("archContrato");
	selEmpresa.className = "form-control modal-input";
	cantContrato.className = "form-control modal-input";
	iniFecha.className = "form-control modal-input";
	finFecha.className = "form-control modal-input";
	archContrato.className = "form-control modal-input";
	selEmpresa.value = "";
	cantContrato.value = "";
	iniFecha.value = "";
	finFecha.value = "";
	archContrato.value = "";

	selEmpresa = document.getElementById("MselEmpresa");
    cantContrato = document.getElementById("McantContrato");
    iniFecha = document.getElementById("MiniFecha");
    finFecha = document.getElementById("MfinFecha");
    archContrato = document.getElementById("MarchContrato");
	let estadoCont = document.getElementById("Mestado");
	selEmpresa.className = "form-control modal-input";
	cantContrato.className = "form-control modal-input";
	iniFecha.className = "form-control modal-input";
	finFecha.className = "form-control modal-input";
	archContrato.className = "form-control modal-input";
	estadoCont.classname = "form-control modal-input";
	selEmpresa.value = "";
	cantContrato.value = "";
	iniFecha.value = "";
	finFecha.value = "";
	archContrato.value = "";
	estadoCont.value = "";
}

function verificarCampos(){
	const selEmpresa = document.getElementById("selEmpresa");
    const cantContrato = document.getElementById("cantContrato");
    const iniFecha = document.getElementById("iniFecha");
    const finFecha = document.getElementById("finFecha");
    const archContrato = document.getElementById("archContrato");
	if (selEmpresa.value=="" || cantContrato.value=="" || iniFecha.value=="" || finFecha.value=="" || !archContrato.files[0]){
		return false;
	}
	return true;
}

function verificarCamposM(){
	const selEmpresa = document.getElementById("MselEmpresa");
    const cantContrato = document.getElementById("McantContrato");
    const iniFecha = document.getElementById("MiniFecha");
    const finFecha = document.getElementById("MfinFecha");
	if (selEmpresa.value=="" || cantContrato.value=="" || iniFecha.value=="" || finFecha.value==""){
		return false;
	}
	return true;
}

function eliminarC(id){
	$.ajax({
		url: "./EliminarContrato.php",
		type: "POST",
		data: {id:id},
		success: function(response){
			const contrato = JSON.parse(JSON.stringify(response));
			document.getElementById("mensajeRes").innerHTML = contrato.mensaje;
			document.getElementById("alertaE").className = "modal-body alert alert-success";
			$("#confirmar").modal("hide");
			$("#respuesta").modal("show");
			$("#respuesta").on('hidden.bs.modal',function(){ 
				location.reload();
			});
		}
	});
}
function confirmacion(id){
	let funcion = "eliminarC("+id+");";
	document.getElementById("botoneliminar").setAttribute("onclick",funcion);
}
