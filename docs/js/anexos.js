$(document).ready(function(){
	$("form").submit(function(e) {
	  e.preventDefault()
	});
	document.getElementById("cantidad_anexo").value = ""; 
	document.getElementById("annexFile").value = ""; 
		
	
	//Envia por POST los input y archivo pdf para crear contrato
	$("#confirmarRegAnnex").click(async function(){
		let formData = new FormData();
		var list = document.getElementById("FormAgregarAnexo").getElementsByClassName("modal-input");
		if (list && list.length>0){
			for (let i=0;i<list.length;i++){
			  formData.append(list[i].id,list[i].value);
			}
		}		
		formData.append("annexFile", annexFile.files[0]);

		const url = './RegistrarAnexos.php';
		const promise = await fetch(url,{
			method: 'POST',
			mode: "no-cors",
			body: formData
		});
      
		//Revisa la respuesta
		if(promise.ok)
		{
			$("#ModalAnnex").modal("hide");
			const json = await promise.json();			
			document.getElementById("mensajeServidor").innerHTML = json.mensaje;
			if(json.estado==0){
				document.getElementById("alertaAnnex").className = "modal-body alert alert-success";
			}
			else if(json.estado==-1){
				document.getElementById("alertaAnnex").className = "modal-body alert alert-danger";
			}			
			$("#RespuestaAnnex").modal("show");
			$("#RespuestaAnnex").on("hidden.bs.modal",function(){ 
				if(json.estado==-1){
					$("#ModalAnnex").modal("show");	
				}
				else if (json.estado==0){
					document.getElementById("cantidad_anexo").value = ""; 
					document.getElementById("annexFile").value = ""; 		
				}
			});
		}
	});
   
});



function cargarAnexos(valor){          
    var table = $("#ModalAnnexTable tbody");
    var formId = document.getElementById("idcontrato");
    $.ajax({
        type: "POST",
        url: "../../app/controllers/CargarAnexos.php",             
        data: {dato:valor},    
        dataType: 'json',         
        success: function(response) {   
                table.empty();
                formId.value=valor;
				if(response!=false){
                	response.forEach(row => table.append("<tr><td>"+row.nombre_anexo+"</td><td>"+row.cantidad_anexo+"</td><td> <a href=\" /archivos/"+row.nombre_anexo+"\" target=\"_blank\" class=\"btn btn-light\" role=\"button\"><i class=\"fas fa-eye\"></i></a> <button id=\"btn-delete\" onclick=\"confirmarDelete("+row.id_anexo+")\" type=\"\" class=\"btn btn-light\" data-toggle=\"modal\" data-target=\"#confirmarA\"><i class=\"far fa-trash-alt\"></i></button></td>  </tr>"));
				}           
        },
        error: function(data){
            table.empty();
            formId.value=valor;
            
       }
    });           
}
function eliminarA(id){
	$.ajax({
		url:"./EliminarAnexo.php",
		type: "POST",
		data:{id:id},
		success: function(response){
			const anexo = JSON.parse(JSON.stringify(response));
			document.getElementById("mensajeResA").innerHTML = anexo.mensaje;
			document.getElementById("alertaA").className = "modal-body alert alert-success";
			console.log(document.getElementById("alertaA"));
			console.log("prueba");
			$("#confirmarA").modal("hide");
			$("#respuestaA").modal("show");
			$("#respuestaA").on('hidden.bs.modal',function(){ 
				cargarAnexos(anexo.contrato);
				$("#ModalAnnex").modal("show");
			});

		}
	})
}
function confirmarDelete(id){
	$("#ModalAnnex").modal("hide");
	let funcion = "eliminarA("+id+");";
	document.getElementById("botoneliminarA").setAttribute("onclick",funcion);
}
