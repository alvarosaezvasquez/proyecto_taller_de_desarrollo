-- phpMyAdmin SQL Dump
-- version 4.6.6deb4+deb9u2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 07-08-2021 a las 19:27:11
-- Versión del servidor: 5.7.28
-- Versión de PHP: 7.0.33-0+deb9u11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `G61tdesarrollo_bd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Anexo`
--

CREATE TABLE `Anexo` (
  `id_anexo` int(11) NOT NULL,
  `id_contrato` int(11) NOT NULL,
  `nombre_anexo` varchar(45) NOT NULL,
  `cantidad_anexo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Anexo`
--

INSERT INTO `Anexo` (`id_anexo`, `id_contrato`, `nombre_anexo`, `cantidad_anexo`) VALUES
(1, 1, '9gm0ddno7zbelhzznfjonnl3.pdf', 1),
(2, 2, '9gm0ddno7zbelhzznfjonnl3.pdf', 10),
(3, 3, '9gm0ddno7zbelhzznfjonnl3.pdf', 12),
(4, 4, '9gm0ddno7zbelhzznfjonnl3.pdf', 5),
(5, 5, '9gm0ddno7zbelhzznfjonnl3.pdf', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Contrato`
--

CREATE TABLE `Contrato` (
  `id_contrato` int(11) NOT NULL,
  `id_empresa` int(11) NOT NULL,
  `nombre_contrato` varchar(45) NOT NULL,
  `fechainicio_contrato` date NOT NULL,
  `fechatermino_contrato` date NOT NULL,
  `estado` varchar(45) NOT NULL,
  `cant_equipos` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Contrato`
--

INSERT INTO `Contrato` (`id_contrato`, `id_empresa`, `nombre_contrato`, `fechainicio_contrato`, `fechatermino_contrato`, `estado`, `cant_equipos`) VALUES
(1, 1, '9gm0ddno7zbelhzznfjonnl3.pdf', '2021-07-30', '2021-08-02', 'terminado', 120),
(2, 2, '9gm0ddno7zbelhzznfjonnl3.pdf', '2021-03-10', '2021-10-12', 'vigente', 50),
(3, 3, '9gm0ddno7zbelhzznfjonnl3.pdf', '2021-05-15', '2021-11-14', 'vigente', 60),
(4, 4, '9gm0ddno7zbelhzznfjonnl3.pdf', '2021-02-25', '2021-09-15', 'vigente', 100),
(5, 5, '9gm0ddno7zbelhzznfjonnl3.pdf', '2021-08-05', '2021-10-30', 'vigente', 150);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Empresa`
--

CREATE TABLE `Empresa` (
  `id_empresa` int(11) NOT NULL,
  `nombre_empresa` varchar(45) NOT NULL,
  `rut_empresa` varchar(45) NOT NULL,
  `telefono_empresa` int(11) NOT NULL,
  `correo_empresa` varchar(45) NOT NULL,
  `sitioweb_empresa` varchar(45) DEFAULT NULL,
  `faltas` int(11) NOT NULL,
  `direccion_empresa` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Empresa`
--

INSERT INTO `Empresa` (`id_empresa`, `nombre_empresa`, `rut_empresa`, `telefono_empresa`, `correo_empresa`, `sitioweb_empresa`, `faltas`, `direccion_empresa`) VALUES
(1, 'EMPRESA 1', '68405714-6', 978661601, 'empresa1@gmail.com', 'http://www.empresa1.cl', 0, 'Direccion 1'),
(2, 'EMPRESA 2', '50280475-8', 926659768, 'empresa2@gmail.com', 'http://www.empresa2.cl', 4, 'Direccion 2'),
(3, 'EMPRESA 3', '90008896-5', 964251344, 'empresa3@gmail.com', 'http://www.empresa3.cl', 2, 'Direccion 3'),
(4, 'EMPRESA 4', '71444860-9', 973659173, 'empresa4@gmail.com', 'http://www.empresa4.cl', 0, 'Direccion 4'),
(5, 'EMPRESA 5', '61696982-k', 976529488, 'empresa5@gmail.com', 'http://www.empresa5.cl', 0, 'Direccion 5');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Anexo`
--
ALTER TABLE `Anexo`
  ADD PRIMARY KEY (`id_anexo`,`id_contrato`),
  ADD KEY `fk_Anexo_Contrato1_idx` (`id_contrato`);

--
-- Indices de la tabla `Contrato`
--
ALTER TABLE `Contrato`
  ADD PRIMARY KEY (`id_contrato`,`id_empresa`),
  ADD KEY `fk_Contrato_Empresa1_idx` (`id_empresa`);

--
-- Indices de la tabla `Empresa`
--
ALTER TABLE `Empresa`
  ADD PRIMARY KEY (`id_empresa`),
  ADD UNIQUE KEY `rut_empresa_UNIQUE` (`rut_empresa`),
  ADD UNIQUE KEY `correo_empresa_UNIQUE` (`correo_empresa`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Anexo`
--
ALTER TABLE `Anexo`
  MODIFY `id_anexo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `Contrato`
--
ALTER TABLE `Contrato`
  MODIFY `id_contrato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `Empresa`
--
ALTER TABLE `Empresa`
  MODIFY `id_empresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `Anexo`
--
ALTER TABLE `Anexo`
  ADD CONSTRAINT `fk_Anexo_Contrato1` FOREIGN KEY (`id_contrato`) REFERENCES `Contrato` (`id_contrato`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Contrato`
--
ALTER TABLE `Contrato`
  ADD CONSTRAINT `fk_Contrato_Empresa1` FOREIGN KEY (`id_empresa`) REFERENCES `Empresa` (`id_empresa`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
