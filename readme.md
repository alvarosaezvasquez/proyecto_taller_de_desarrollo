# Software de Gestión de Contratos Universidad del Sur
Sistema web basado en PHP y con un motor de base de datos MySQL que permite gestionar el proceso de arriendo de computadores para facultades y académicos de la Universidad del Sur, facilitando el registro de empresas licitantes y oferentes, además que permite llevar un registro de los contratos y administrarlos.

## Restauracion Base de datos
Este sistema requiere de una base de datos mysql para funcionar; dentro de la carpeta **gestioncontratos/sql** se encuentra un archivo llamado **Respaldo.sql** , que contiene las llamadas para crear las tablas necesarias, junto a inserciones de prueba; el archivo deben ser importado al servidor que utiliza en su ambiente (ya sea producción o desarrollo) como estime conveniente.

## Configuración de ejecución para entorno de Producción
## Software Requerido
* Servidor con SO compatible con Debian, sistema desarrollado en la versión 10-buster.
* Servidor Web apache 2.4.x
* PHP 7.3 , junto al modulo php-mysql
* Composer
* git
* Base de datos MySQL

## Instrucciones
Desde la terminal de Linux, instalar **apache2, php7.3 y  php-mysql**, con el comando sudo de ser necesario, o permisos de administrador:
`apt-get update && apt-get install -y apache2 php7.3 php-mysql `

Luego, se necesita que la zona horaria local sea **America/Santiago** y la codificación de caracteres **es_CL.UTF-8** , para esto se ejecuta el siguiente comando que también instala **vim, locales y  wget** 

    apt-get install -y vim locales wget &&\
    rm /etc/localtime && \
        ln -snf /usr/share/zoneinfo/America/Santiago /etc/localtime && \
        echo "America/Santiago" > /etc/timezone && \
        echo "LC_ALL=es_CL.UTF-8" >> /etc/environment && \
        echo "es_CL.UTF-8 UTF-8" >> /etc/locale.gen && \
        echo "LANG=es_CL.UTF-8" > /etc/locale.conf && \
        locale-gen es_CL.UTF-8 && \
        dpkg-reconfigure -f noninteractive tzdata 

A continuación, instalar **Composer:**

    wget -O /usr/local/bin/composer https://getcomposer.org/download/latest-stable/
    composer.phar && chmod +x /usr/local/bin/composer && apt-get clean

Ahora instalar **git**:

    apt-get install git-all

Ahora, obtenemos una copia del proyecto mediante **git clone**; asumiendo que estamos ubicados en la carpeta de usuario desde nuestra terminal Linux:
`git clone http://gitlabtrans.face.ubiobio.cl:8081/18430958/gestioncontratos.git`
el sistema pedirá sus credenciales de la pagina gitlabtrans.face.ubiobio.cl para poder realizar esta acción.

Luego, movemos el archivo *php.ini*, ubicado en la carpeta **/Dockerfile** del proyecto, a la carpeta de apache  /etc/php/7.3/apache2 .
`mv gestioncontratos/Dockerfile/php.ini /etc/php/7.3/apache2 `

Luego, creamos el archivo *credenciales.json* en la carpeta de nuestro proyecto, en *app/models*, con el siguiente contenido:

    {
	    "servername":"BD",
	    "dbname":"NAME",
	    "username":"USER",
	    "password" : "PASS"   
	}
Completando los campos con las credenciales correspondientes al servidor de base de datos a utilizar en producción.

Luego, copiamos el contenido de gestioncontratos a la carpeta **declarada en su DocumentRoot de apache**.

Después, otorgamos permisos de lectura y escritura (666) a la carpeta **archivos** (junto a su contenido) dentro del proyecto, ahora ubicado en su DocumentRoot definido.

Luego, editamos el archivo */etc/apache2/apache2.conf*,  de forma que el siguiente campo quede así:

    <Directory [DocumentRoot definido]>
	    Options Indexes FollowSymLinks
	    AllowOverride All
	    Require all granted
    </Directory>
Esto permitirá el uso correcto del archivo *.htaccess*, denegando el indexado de directorios de apache y la navegación por los directorios y archivos del proyecto.

Luego, utilizamos **composer dump-autoload** desde el directorio del proyecto ubicado en su DocumentRoot, donde se encuentra *composer.json* , para cargar las dependencias del proyecto.

Ahora para la ejecución de la aplicación web y el servidor, debemos iniciar el servicio de apache; si se encuentra en linux, utilizamos el comando:
`/etc/init.d/apache2 start `  , si el servicio se encuentra apagado
`/etc/init.d/apache2 restart `  , si el servicio se encontraba activo

Finalmente, dirigirse a su dirección web para verificar el funcionamiento de la pagina.


## Configuración de ejecución para entorno de Desarrollo
## Software Requerido
* Docker
* git
## Instrucciones
Clonar este repositorio al entorno de desarrollo a utilizar:
`git clone http://gitlabtrans.face.ubiobio.cl:8081/18430958/gestioncontratos.git`
	el sistema pedirá sus credenciales de la pagina gitlabtrans.face.ubiobio.cl para poder realizar esta acción.

Luego, ir a **gestioncontratos/Dockerfile** y ejecutar el siguiente código en consola para generar la imagen docker:

    docker build -t name:tag ./
con los name y tag que estime conveniente.

A continuación correr la imagen generada en un contenedor docker utilizando:

    docker run --rm -d -p 80:80 -v '[ruta en el host]:/var/www/html/' name:tag

[ruta en el host] debe ser reemplazado por la ruta donde manipulará el contenido de **gestioncontratos** descargado de gitlab en el sistema host.

Luego, creamos el archivo *credenciales.json* en la carpeta de nuestro proyecto, en *app/models*, con el siguiente contenido:

    {
	    "servername":"BD",
	    "dbname":"NAME",
	    "username":"USER",
	    "password" : "PASS"   
	}
Completando los campos con las credenciales correspondientes al servidor de la base de datos utilizado en el ambiente de desarrollo.
Si no dispone de las credenciales de este servidor, hospedado en mysqltrans.face.ubiobio.cl , por favor contactarse con:

 - Alvaro Sáez, Ingeniero de BD alavaro.saez1801@alumnos.ubiobio.cl
 - Giordano Salini, Líder del proyecto giordano.salini1801@alumnos.ubiobio.cl

Después, otorgamos permisos de lectura y escritura (666) a la carpeta **archivos**(junto a su contenido) ubicada en la raíz del proyecto,  en este caso /var/www/html dentro del contenedor.

Finalmente ejecutar en la carpeta /var/www/html del contenedor desde la consola de este:

    composer dump-autoload

Corroborar que el servicio de apache este activo; si no lo esta, activarlo desde la consola del contenedor:

    /etc/init.d/apache2 start 

*recordar que se puede acceder a la consola del contenedor utilizando el siguiente comando desde el host:

    docker exec -it nombre_contenedor /bin/bash


Se puede verificar el funcionamiento de la pagina desde http://localhost/ 