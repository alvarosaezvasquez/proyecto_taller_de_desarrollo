 <!-- cabezera -->
 <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
      <a class="navbar-brand bg-light logo-uds" href="index.php">
        <img class="" src="../../docs/img/uds.PNG" height="85" width="auto" class="d-inline-block align-center" alt="universidad del sur">
      </a>
      <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav w-100">
          <li class="ml-auto"></li>
          <li class="nav-item">
            <a class="nav-link" href="ListarContratos.php"><i class="navico fas fa-file-signature fa-2x"></i>Contratos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="ListarEmpresas.php"><i class="navico fas fa-building fa-2x"></i>Empresas</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"><i class="navico far fa-user fa-2x"></i>Nombre</a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <li>
                <a class="dropdown-item navdrop" href="#">Rol:Soporte</a>
              </li>
              <li>
                <a class="dropdown-item navdrop" href="#">Configuracion</a>
              </li>
              <li>
                <a class="dropdown-item navdrop" href="#">Cerrar sesion</a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <div class="nav-empty"></div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">            
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    </div>
  </nav>
  <div class="container-fluid">
    <div class="row">
      <!-- Barra lateral -->