<?php
    namespace app\models;
    
    require_once $_SERVER["DOCUMENT_ROOT"].'/vendor/autoload.php';
    use app\models\Dbh;
    use PDO;
    class Contratos extends Dbh{ 

        // Obtiene un contrato a partir de una id
        public function getContrato($id){
            try {
                $conn = $this->connect();
                $sql = "SELECT * FROM Contrato WHERE id_contrato=:id;";

                $resultado = $conn->prepare($sql);
                $resultado->bindParam(':id',$id);
                $resultado->execute();
                if($resultado->rowCount()>0){
                    $contrato = $resultado->fetch(PDO::FETCH_OBJ);
                    return $contrato;
                }else{
                    return false;
                }
            }catch(PDOexception $e){
                return false;
            }
        }

        // Obtiene todos los contratos desde bd
        public function getContratos(){
            try {
                $conn = $this->connect();
                $sql = "SELECT * FROM Contrato;";

                $resultado = $conn->prepare($sql);
                $resultado->execute();
                if($resultado->rowCount()>0){
                    $contratos = $resultado->fetchAll(PDO::FETCH_OBJ);
                    return $contratos;
                }else{
                    return false;
                }
            }catch(PDOexception $e){
                return false;
            }
        }

        // Crea un nuevo registro de contrato
        public function postContrato($id_e,$fname,$date_i,$date_f,$stat,$qn){
            try {
                $conn = $this->connect();
                $sql = "INSERT INTO Contrato(id_empresa,nombre_contrato,fechainicio_contrato,fechatermino_contrato,estado,cant_equipos)
                        VALUES (:id_e,:fname,:date_i,:date_f,:stat,:qn);";

                $resultado = $conn->prepare($sql);
                $resultado->bindParam(':id_e',$id_e);
                $resultado->bindParam(':fname',$fname);
                $resultado->bindParam(':date_i',$date_i);
                $resultado->bindParam(':date_f',$date_f);
                $resultado->bindParam(':stat',$stat);
                $resultado->bindParam(':qn',$qn);
                $resultado->execute();
                return true;
            }catch(PDOexception $e){
                return false;
            }
        }

        // Modifica un registro de contrato
        public function putContrato($id_c,$nombre_contrato,$id_e,$date_i,$date_f,$stat,$qn){
            try {
                $conn = $this->connect();
                $sql = "UPDATE Contrato
                        SET id_empresa=:id_e,
                            fechainicio_contrato=:date_i,
                            fechatermino_contrato=:date_f,
                            estado=:stat,
                            cant_equipos=:qn
                        WHERE id_contrato=:id_c;";

                $resultado = $conn->prepare($sql);
                $resultado->bindParam(':id_c',$id_c);
                $resultado->bindParam(':id_e',$id_e);
                $resultado->bindParam(':date_i',$date_i);
                $resultado->bindParam(':date_f',$date_f);
                $resultado->bindParam(':stat',$stat);
                $resultado->bindParam(':qn',$qn);
                $resultado->execute();
                return true;
            }catch(PDOexception $e){
                return false;
            }
        }
        
        public function deleteContrato($id_contrato){
            try{
                $conn = $this->connect();
                $sql = $conn->prepare("DELETE FROM Contrato where Contrato.id_contrato = ?;");
                $sql->execute([$id_contrato]);
                return true;
            }catch(PDOexception $e){
                return false;
            }
        }
    }

?>

