<?php
    namespace app\models;
    use PDO;
    class Dbh{     
      protected function connect(){
        try {
          $string = file_get_contents("../models/credenciales.json");
          $json = json_decode($string, true);
          $servername = $json['servername']; 
          $dbname= $json['dbname'];  
          $username =  $json['username']; 
          $password = $json['password']; 
          $conn = new PDO("mysql:host=".$servername.";dbname=".$dbname, $username, $password);
          $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
          return $conn;
        } catch(PDOException $e) {
          echo "Connection failed: " . $e->getMessage();
        }
      }
    }

?>
