<?php
namespace app\models;
require_once '../../vendor/autoload.php';
use app\models\Dbh;
use PDO;
    class Empresas extends Dbh{

        //Obtiene una empresa a partir de una id
        public function getEmpresa($id_empresa){
            try {
                $conn = $this->connect();
                $resultado = $conn->prepare("SELECT * FROM Empresa WHERE id_empresa=:id_empresa;");
                $resultado->bindParam(":id_empresa",$id_empresa);
                $resultado->execute();
                if($resultado->rowCount()>0){
                    $empresa = $resultado->fetchAll(PDO::FETCH_OBJ);
                    return $empresa;
                }else{
                    return false;
                }
            }catch(PDOexception $e){
                return false;
            }
        }

        //Obtiene todas las empresas de la bd
        public function getEmpresas(){
            try{
                $conn = $this->connect();
                $resultado = $conn->prepare("SELECT * FROM Empresa;");
                $resultado->execute();
                if($resultado->rowCount()>0){
                    $empresa = $resultado->fetchAll(PDO::FETCH_OBJ);
                    return $empresa;
                }else{
                    return false;
                }
            }catch(PDOException $e){
                return false;
            }
        }

        //Obtiene la empresa por el rut
        public function getEmpresaByRut($rut_empresa) {
            try {
                $conn = $this->connect();
                $resultado = $conn->prepare("SELECT * FROM Empresa WHERE rut_empresa=:rut_empresa;");
                $resultado->bindParam(":rut_empresa",$rut_empresa);
                $resultado->execute();
                if($resultado->rowCount()>0){
                    $empresa = $resultado->fetchAll(PDO::FETCH_OBJ);
                    return $empresa;
                }else{
                    return false;
                }
            }catch(PDOexception $e){
                return false;
            }
        }

        public function getEmpresaByCorreo($correo_empresa) {
            try {
                $conn = $this->connect();
                $resultado = $conn->prepare("SELECT * FROM Empresa WHERE correo_empresa=:correo_empresa;");
                $resultado->bindParam(":correo_empresa",$correo_empresa);
                $resultado->execute();
                if($resultado->rowCount()>0){
                    $empresa = $resultado->fetchAll(PDO::FETCH_OBJ);
                    return $empresa;
                }else{
                    return false;
                }
            }catch(PDOexception $e){
                return false;
            }
        }

        //Crea un registro nuevo de empresa

        public function postEmpresa($nombre_empresa, $rut_empresa, $correo_empresa, $telefono_empresa, $sitioweb_empresa, $direccion_empresa){
            try {
                $conn = $this->connect();
                $stmt = $conn->prepare("INSERT INTO Empresa(nombre_empresa, rut_empresa, correo_empresa, telefono_empresa, sitioweb_empresa, faltas, direccion_empresa)
                                    VALUES (:nombre_empresa, :rut_empresa, :correo_empresa, :telefono_empresa, :sitioweb_empresa, :faltas, :direccion_empresa)");

                $stmt->bindParam(':nombre_empresa',$nombre_empresa);
                $stmt->bindParam(':rut_empresa',$rut_empresa);
                $stmt->bindParam(':correo_empresa',$correo_empresa);
                $stmt->bindParam(':telefono_empresa',$telefono_empresa);
                $stmt->bindParam(':sitioweb_empresa',$sitioweb_empresa);
                $faltas = '0';
                $stmt->bindParam(':faltas', $faltas); 
                $stmt->bindParam(':direccion_empresa',$direccion_empresa);
                $stmt->execute();
                
                return true;
            } catch (PDOexception $e) {
                return false;
            }
        }

        //Modifica un registro de empresas
        public function putEmpresa($id_empresa, $nombre_empresa, $rut_empresa, $correo_empresa, $telefono_empresa, $sitioweb_empresa,$faltas, $direccion_empresa){
            try {
                $conn = $this->connect();
                $stmt = $conn->prepare ("UPDATE Empresa
                        SET id_empresa=:id_empresa,
                            nombre_empresa=:nombre_empresa,
                            rut_empresa=:rut_empresa,
                            correo_empresa=:correo_empresa,
                            telefono_empresa=:telefono_empresa,
                            sitioweb_empresa=:sitioweb_empresa,
                            faltas=:faltas,
                            direccion_empresa=:direccion_empresa
                        WHERE id_empresa=:id_empresa");

                
                $stmt->bindParam(':id_empresa', $id_empresa);
                $stmt->bindParam(':nombre_empresa',$nombre_empresa);
                $stmt->bindParam(':rut_empresa',$rut_empresa);
                $stmt->bindParam(':correo_empresa',$correo_empresa);
                $stmt->bindParam(':telefono_empresa',$telefono_empresa);
                $stmt->bindParam(':sitioweb_empresa',$sitioweb_empresa);
                $stmt->bindParam(':direccion_empresa',$direccion_empresa);
                $stmt->bindParam(':faltas', $faltas); 
                $stmt->execute();
                return true;
            }catch(PDOexception $e){
                return false;
            }
        }

    }

?>