<?php
    namespace app\models;

    require_once $_SERVER["DOCUMENT_ROOT"].'/vendor/autoload.php';
    use app\models\Dbh;
    use PDO;
    class Anexos extends Dbh{         

         function crearAnexo($nombreAnexo,$cantidadAnexo,$idContrato){
            $conn = $this->connect();
            $stmt=$conn->prepare("INSERT INTO Anexo(nombre_anexo,cantidad_anexo,id_contrato) VALUES (?, ?, ?);");
            $result = $stmt->execute([$nombreAnexo,$cantidadAnexo,$idContrato]);
            return true;
        }
        
        protected function nombreAnexo($idAnexo){
            $conn = $this->connect();
            $stmt=$conn->prepare("SELECT * FROM Anexo WHERE id_anexo = ?;");
            $stmt->execute([$idAnexo]);
            if( $result = $stmt->fetch()){                
                return $result["nombre_anexo"];}
            else{$result=false;
                return $result;}
        }
        // Jose, te cambie la funcion, quite el protected y al fetch agregue PDO::FETCH_OBJ
        function getAnexo($idAnexo){
            $conn = $this->connect();
            $stmt=$conn->prepare("SELECT * FROM Anexo WHERE id_anexo = ?;");
            $stmt->execute([$idAnexo]);
            if( $result = $stmt->fetch(PDO::FETCH_OBJ)){                
                return $result;}
            else{$result=false;
                return $result;}
        }
        protected function getAnexosId(){
            $conn = $this->connect();
            $stmt = $conn->query("SELECT * FROM Anexo ORDER BY id_anexo DESC");            
            if( $result = $stmt->fetchAll()){                
                return $result;}
            else{$result=false;
                return $result;}
        }
        // Jose, te cambie la funcion, quite el protected y al fetch agregue PDO::FETCH_OBJ
        function getAnexosContrato($idContrato){
            $conn = $this->connect();
            $stmt = $conn->prepare("SELECT * FROM Anexo WHERE id_contrato = ? ORDER BY id_anexo DESC");    
            $stmt->execute([$idContrato]);        
            if( $result = $stmt->fetchAll(PDO::FETCH_OBJ)){                
                return $result;}
            else{$result=false;
                return $result;}
        }

           // Elimina los anexos del contrato asociado 
            public function deleteContratoAnexo($id_contrato){
            try{
                $conn = $this->connect();
                $sql = $conn->prepare("DELETE FROM Anexo WHERE Anexo.id_contrato = ?;");
                $sql->execute([$id_contrato]);
                return true;
            }catch(PDOexception $e){
                return false;
            }
        }
        
            //Elimina el anexo especifico
        public function deleteAnexo($id_anexo){
            try{
                $conn = $this->connect();
                $sql = $conn->prepare("DELETE FROM Anexo WHERE Anexo.id_anexo = ?;");
                $sql->execute([$id_anexo]);
                return true;
            }catch(PDOexception $e){
                return false;
            } 
        }
    
        function sumaCantidadAnexos($idContrato){
            $conn = $this->connect();
            $stmt = $conn->prepare("SELECT SUM(cantidad_anexo) FROM Anexo WHERE id_contrato = ?");    
            $stmt->execute([$idContrato]);        
            if( $result = $stmt->fetch(PDO::FETCH_NUM)){                
                return $result[0];}
            else{$result="false";
                return $result;}
        }
    }

?>

