<?php
    namespace app\views;
    require_once $_SERVER["DOCUMENT_ROOT"].'/vendor/autoload.php';
    use app\models\Anexos;
    
        class AnexosView extends Anexos{         
        
        function AnexosModal(){
            
            echo <<< EOT
            <!--Inicio Modal tabla-agregar anexos-->
                <div class="modal fade" id="ModalAnnex" tabindex="-1" role="dialog" aria-labelledby="Anexos" aria-hidden="true">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="Anexos">Anexos</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span style="color: white;" aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                    <table class="table table-bordered" id="ModalAnnexTable">
                    <thead class="table-dark">
                        <tr>                      
                            <th>Nombre Anexo</th>
                            <th>Cantidad equipos</th>
                            <th>Accion</th>                      
                        </tr>
                    </thead>
                    <tbody>
            
                    </tbody>
                    </table> 
                <h4>Agregar nuevo anexo:</h4>
                  <form id="FormAgregarAnexo" method="post" enctype="multipart/form-data">
                    <div class="form-group">                       
                        <label for="annexFile">Archivo a subir</label>
                        <input type="file" class="form-control" id="annexFile" name="annexFile" required />
                    </div>
                    <div class="form-group">                     
                        <input type="hidden" id="idcontrato" class="modal-input" name="idcontrato" value="0" />
                    </div>
                    <div class="form-group">
                      <label for="cantidad_anexo">Cantidad equipos extra</label>
                      <input type="number"  class="form-control modal-input" id="cantidad_anexo" name="cantidad_anexo">
                    </div>
                
                    <button id="confirmarRegAnnex" type="submit" name="submit" class="btn btn-success submitBtn" >Agregar anexo</button>
                  </form>
                  
                </div>
                <div class="modal-footer">                  
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
                </div>
                </div>
                </div>
            <!--Fin Modal tabla-agregar anexos-->
            <!--Inicio Modal alerta agregar anexos-->
                <div class="modal fade" id="RespuestaAnnex" role="dialog">
                <div class="modal-dialog" style="top:20%">
                    <div class="modal-content" style="margin-bottom:0;">
                        <div id="alertaAnnex" class="modal-body" role="alert" style="position: relative; margin: 8px;">
                        <button type="button" id="close-button" class="close" data-dismiss="modal">&times;</button>
                        <div id="mensajeServidor" class="form-group" style="padding-top: 10px; margin: 0;">
                        
                        </div> 
                    </div>
                    </div>
                </div>
                </div>
            <!--Fin Modal alerta agregar anexos-->
            <!--Inicio modal de confirmacion de Eliminar Anexo-->
                <div class="modal fade" id="confirmarA" role="dialog">
                    <div class="modal-dialog" style="top:20%">
                        <div class="modal-content" style="margin-bottom:0;">
                        <div class="modal-header">
                        <h5 class="modal-title" id="alertaModal">¿Seguro de eliminar el Anexo seleccionado?</h5></div>
                            <div id="alerta" class="modal-body" role="alert" style="position: relative; margin: 8px;">
                             
                            <button type="button" onclick= "eliminarA()" id= "botoneliminarA" class="btn btn-dark">Eliminar</button>
                            <button type="button" onclick= '$("#ModalAnnex").modal("show");' class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        </div>
                        </div>
                    </div>
                </div>
            <!-- Fin modal de confirmacion de Eliminar Anexo -->

            <!-- Inicio modal de respuesta de Eliminar Anexo -->
            <div class="modal fade" id="respuestaA" role="dialog">
                <div class="modal-dialog" style="top:20%">
                    <div class="modal-content" style="margin-bottom:0;">
                        <div id="alertaA" class="modal-body" role="alert" style="position: relative; margin: 8px;">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <div id="mensajeResA" class="form-group" style="padding-top: 10px; margin: 0;">fffff</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--- Fin modal de respuesta de Eliminar Anexo -->
            EOT ;
        }
        
    }


?>

