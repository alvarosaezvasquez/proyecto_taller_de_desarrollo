<?php
namespace app\views;
require_once '../../vendor/autoload.php';
use app\controllers\ListarEmpresas;
use app\controllers\registrarEmpresa;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.bootstrap4.min.css">
    <script src="https://kit.fontawesome.com/4731a58e4b.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../../docs/css/style.css"></script>
    <link rel="stylesheet" href="../../docs/css/datatable.css"></script>
    <title>Empresas</title>
</head>
<body>
  <!-- cabezera -->
  <?php require_once '../includes/header.php'?>
        
        <!-- Contenido -->
        <!-- Inicio Datatable -->
        <div class="col-sm-12 col-md-12 col-ld-12 container">
          <button id="btnReg" type="button" class="btn btn-dark float-right btn-nuevo" data-toggle="modal" data-target="#modalNuevo">Nuevo</button>
            <h1 class="titulo-1">Empresas</h1><br/>
            <table class="table table-bordered" id="mydatatable">
              <thead class="table-dark">
                <tr>
                  <th>Nombre</th>
                  <th>Rut</th>
                  <th>Dirección</th>
                  <th>E-mail</th>
                  <th>Teléfono</th>
                  <th>Sitio Web</th>
                  <th>Faltas</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody id="empresas">
                
                <?php if($empresas): foreach($empresas as $row): ?>
                  <tr id-empresas="<?= $row->id_empresa?>">
                    <td><?= $row->nombre_empresa?></td>
                    <td><?= $row->rut_empresa?></td>
                    <td><?= $row->direccion_empresa?></td>
                    <td><?= $row->correo_empresa?></td>
                    <td><?= $row->telefono_empresa?></td>
                    <td><?= $row->sitioweb_empresa?></td>
                    <td><?= $row->faltas?></td>
                    <td>
                      <button  id="btnMod" type="button" class="" data-toggle="modal" data-target="#modalEdicion" onclick=""><i class="far fa-edit"></i></button>
                    </td>
                  </tr>
                <?php endforeach; endif; ?>
              </tbody>
            </table>
        
        <!-- Fin Datatable -->

        <!-- Modal Registrar -->
          <div class="modal fade" id="modalNuevo" tabindex="-1" role="dialog" aria-labelledby="Registrar" aria-hidden="true" data-backdrop="static">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="Registrar">Registrar Empresa</h5>
                  <button type="button" id="close-button" onclick="limpiarFormNuevo(); limpiarFormEdicion()" class="close" data-dismiss="modal" aria-label="Cerrar">
                    <span style="color: white;" aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <!-- Formulario -->
                  <form id="register-form">
                    <div class="form-group">
                      <p>(*) Campos obligatorios </p>
                    </div>
                    <div class="form-group">
                        <label for="nombreEmpresa">Nombre de la empresa (*)</label>
                        <input type="text" class="form-control" id="nombreEmpresa" name="nombreEmpresa" required>
                    </div>
                    <div class="form-group">
                        <label for="rutEmpresa">RUT de la empresa (*)</label>
                        <input type="text" class="form-control" id="rutEmpresa" name="rutEmpresa" required oninput="checkRut(this)">
                    </div>
                    <div class="form-group">
                      <label for="dirEmpresa">Dirección (*)</label>
                      <input type="text" class="form-control" id="dirEmpresa" name="dirEmpresa" required>
                    </div>
                    <div class="form-group">
                      <label for="emailEmpresa">E-mail de la empresa (*)</label>
                      <input type="text" class="form-control" id="emailEmpresa" name="emailEmpresa" placeholder="ejemplo: miempresa@empresa.com" required oninput="validarEmail(this)">
                    </div>
                    <div class="form-group">
                      <label for="telefonoEmpresa">Teléfono (*)</label>
                      <input type="number" class="form-control" id="telefonoEmpresa" name="telefonoEmpresa" required>
                    </div>
                    <div class="form-group">
                      <label for="webEmpresa">Sitio Web</label>
                      <input type="text" class="form-control" id="webEmpresa" name="webEmpresa" placeholder="https://www.ejemplo.com" oninput="validarUrl(this)">
                    </div>
                    <div class="modal-footer">
                      <button type="submit" id="confirmar-registro" class="btn btn-primary btn-dark">Confirmar</button>
                      <button type="button" id="btn-cancel" onclick="limpiarFormNuevo(); limpiarFormEdicion()" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                  </form>
                  <!-- Fin formulario -->
                </div>
              </div>
            </div>
          </div>
          <!-- Fin Modal -->

          <!-- Modal Modificar -->
          <div class="modal fade" id="modalEdicion" tabindex="-1" role="dialog" aria-labelledby="Modificar" aria-hidden="true" data-backdrop="static">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="Modificar">Modifcar Empresa</h5>
                  <button type="button" id="close-button" onclick="limpiarFormEdicion(); limpiarFormNuevo()" class="close" data-dismiss="modal" aria-label="Cerrar">
                    <span style="color: white;" aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <!-- Formulario -->
                  <form id="modify-form">
                    <div class="form-group">
                      <p>(*) Campos obligatorios </p>
                    </div>
                    <div>
                    <input type="hidden" id="idMod" name="idMod">
                    </div>
                    <div class="form-group">
                      <label for="nombreEmpresaMod">Nombre de la empresa (*)</label>
                      <input type="text" class="form-control" id="nombreEmpresaMod" name="nombreEmpresaMod">
                    </div>
                    <div class="form-group">
                        <label for="rutEmpresaMod">RUT de la empresa (*)</label>
                        <input type="text" class="form-control" id="rutEmpresaMod" name="rutEmpresaMod" oninput="checkRut(this)">
                    </div>
                    <div class="form-group">
                      <label for="dirEmpresaMod">Dirección (*)</label>
                      <input type="text" class="form-control" id="dirEmpresaMod" name="dirEmpresaMod">
                    </div>
                    <div class="form-group">
                      <label for="emailEmpresaMod">E-mail de la empresa (*)</label>
                      <input type="text" class="form-control" id="emailEmpresaMod" name="emailEmpresaMod" oninput="validarEmail(this)">
                    </div>
                    <div class="form-group">
                      <label for="telefonoEmpresaMod">Teléfono (*)</label>
                      <input type="number" class="form-control" id="telefonoEmpresaMod" name="telefonoEmpresaMod">
                    </div>
                    <div class="form-group">
                      <label for="webEmpresaMod">Sitio Web</label>
                      <input type="text" class="form-control" id="webEmpresaMod" name="webEmpresaMod" placeholder="https://www.ejemplo.com">
                    </div>
                    <div class="form-gorup">
                      <label for="faltaEmpresaMod">Faltas</label>
                      <input type="number" class="form-control" id="faltasMod" name="faltasMod">
                    </div>
                    <div class="modal-footer">
                      <button type="submit" id="confirmar-modificacion" class="btn btn-dark">Confirmar</button>
                      <button type="button" id="cancel-mod" onclick="limpiarFormEdicion(); limpiarFormNuevo()" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                  </form>
                  <!-- Fin formulario -->
                </div>
              </div>
            </div>
          </div>
          <!-- Fin Modal -->

        <!--Inicio modal de respuesta -->
        <div class="modal fade" id="Respuesta" role="dialog">
          <div class="modal-dialog" style="top:20%">
            <div class="modal-content" style="margin-bottom:0;">
              <div id="alerta" class="modal-body" role="alert" style="position: relative; margin: 8px;">
                <button type="button" id="close-button" class="close" data-dismiss="modal">&times;</button>
                <div id="mensajeServidor" class="form-group" style="padding-top: 10px; margin: 0;">
                </div> 
              </div>
            </div>
          </div>
        </div>
        <!-- Fin modal de respuesta -->
          
        </div>   
      </div>    
    </div>
    <!-- Pie de pagina -->
    <?php require_once '../includes/footer.php'?>
  <!-- Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.5.1.js" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

  <!-- Datatables JS -->
  <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.bootstrap4.min.js"></script>
  <script src="../../docs/js/datatableTrad.js"></script>
  <script src="../../docs/js/empresas.js"></script>

</body>
</html>