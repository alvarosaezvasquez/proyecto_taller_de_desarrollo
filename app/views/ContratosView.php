<?php
namespace app\views;
require_once '../../vendor/autoload.php';
use app\controllers\ListarContratos;
use app\views\AnexosView;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.bootstrap4.min.css">
    <script src="https://kit.fontawesome.com/4731a58e4b.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../../docs/css/style.css"></script>
    <link rel="stylesheet" href="../../docs/css/datatable.css"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="../../docs/js/anexos.js"></script>
    <title>Contratos</title>
</head>
<body>
  <!-- cabezera -->
  <?php require_once '../includes/header.php'?>
      
      <!-- Contenido -->
      <!-- Inicio Datatable -->
      <div class="col-sm-12 col-md-12 col-ld-12 container">
        <button id="btnReg" type="button" class="btn btn-dark float-right btn-nuevo" data-toggle="modal" data-target="#ModalReg">Nuevo</button>
        <h1 class="titulo-1">Contratos</h1><br/>
        <table class="table table-bordered" id="mydatatable">
          <thead class="table-dark">
            <tr>
              <th>Empresa</th>
              <th>Fecha de inicio</th>
              <th>Fecha de término</th>
              <th>Estado</th>
              <th>Cantidad de equipos</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
          <?php if($contratos): foreach($contratos as $row): ?>
              <tr>
                <td><?php
                  foreach($empresas as $row2):
                    if($row->id_empresa==$row2->id_empresa):
                      echo($row2->nombre_empresa);
                      break;
                    endif;
                  endforeach;
                ?></td>
                <?php $newFechaInicio = date("d-m-Y", strtotime($row->fechainicio_contrato));?>
                <td><?= $newFechaInicio?></td>
                <?php $newFechaTermino = date("d-m-Y", strtotime($row->fechatermino_contrato));?>
                <td><?= $newFechaTermino?></td>
                <td><?= $row->estado?></td>
                <td><?= $row->cant_equipos?></td>
                <td>
                  <button onClick="javascript:window.open('/archivos/'+'<?= $row->nombre_contrato?>', '_blank');"   type="button"><i class="fas fa-eye"></i></button>                  
                  <button type="" onclick= "confirmacion(<?= $row->id_contrato?>)"class="" data-toggle="modal" data-target="#confirmar"><i class="far fa-trash-alt"></i></button>
                  <button type="" class="" data-toggle="modal" data-target="#ModalMod" onclick="cargarContrato(<?= $row->id_contrato?>)"><i class="fas fa-edit"></i></button>
                  <button type="" class="" data-toggle="modal" data-target="#ModalAnnex" onclick="cargarAnexos(<?= $row->id_contrato?>)"><i class="fas fa-paperclip"></i></button>
                </td>
              </tr>
          <?php endforeach; endif; ?>
          </tbody>
        </table>
      </div>
      <!-- Fin Datatable -->

      <!-- Modal Registrar -->
      <div class="modal fade" id="ModalReg" tabindex="-1" role="dialog" aria-labelledby="Registrar" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="Registrar">Registrar Contrato</h5>
              <button type="button" id="close-button" onclick="limpiarForm()" class="close" data-dismiss="modal" aria-label="Cerrar">
                <span style="color: white;" aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <!-- Formulario -->
              <form id="form-Reg" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="archContrato">Archivo de contrato</label>
                    <input type="file" class="form-control" id="archContrato" name="archContrato">
                    <small id="fileHelp" class="form-text text-muted">Solo se permite archivo pdf (Max 40mb)</small>
                </div>
                <div class="form-group">
                    <label for="selEmpresa">Empresa oferente</label>
                    <select class="form-control" id="selEmpresa" name="selEmpresa">
                    <?php if($empresas): foreach($empresas as $row): ?>
                      <option value=<?=$row->id_empresa ?>><?=$row->nombre_empresa ?></option>
                      <?php endforeach; endif;?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="cantContrato">Cantidad de Equipos</label>
                    <input type="number" class="form-control modal-input" id="cantContrato" name="cantContrato">
                </div>
                <div class="form-group">
                    <label for="iniFecha">Fecha de inicio</label>
                    <input type="date" class="form-control modal-input" id="iniFecha" name="iniFecha" rows="3"></textarea>
                </div>
                <div class="form-group">
                  <label for="finFecha">Fecha de vencimiento</label>
                  <input type="date" class="form-control modal-input" id="finFecha" name="finFecha" rows="3"></textarea>
                </div>
              </form>
              <!-- Fin formulario -->
            </div>
            <div class="modal-footer">
              <button id="confirmarReg" type="submit" class="btn btn-dark">Confirmar</button>
              <button type="button" class="btn btn-secondary" onclick="limpiarForm()" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>
      <!-- Fin Modal -->

      <!-- Modal Modificar -->
      <div class="modal fade" id="ModalMod" tabindex="-1" role="dialog" aria-labelledby="Modificar" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="Modificar">Modificar Contrato</h5>
              <button type="button" id="close-button" onclick="limpiarForm()" class="close" data-dismiss="modal" aria-label="Cerrar">
                <span style="color: white;" aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <!-- Formulario -->
              <form id="form-Mod" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="MarchContrato">Archivo de contrato</label>
                    <input type="file" class="form-control" id="MarchContrato" name="MarchContrato">
                    <small id="fileHelp" class="form-text text-muted">Solo se permite archivo pdf (Max 40mb)</small>
                </div>
                <div class="form-group">
                    <label for="MselEmpresa">Empresa oferente</label>
                    <select class="form-control modal-input" id="MselEmpresa" name="MselEmpresa">
                      <?php if($empresas): foreach($empresas as $row): ?>
                      <option value=<?=$row->id_empresa ?>><?=$row->nombre_empresa ?></option>
                      <?php endforeach; endif;?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="McantContrato">Cantidad de Equipos</label>
                    <input type="number" class="form-control modal-input" id="McantContrato" name="McantContrato">
                </div>
                <div class="form-group">
                    <label for="MiniFecha">Fecha de inicio</label>
                    <input type="date" class="form-control modal-input" id="MiniFecha" name="MiniFecha" rows="3"></textarea>
                </div>
                <div class="form-group">
                  <label for="MfinFecha">Fecha de vencimiento</label>
                  <input type="date" class="form-control modal-input" id="MfinFecha" name="MfinFecha" rows="3"></textarea>
                </div>
                <div class="form-group">
                    <input id="Mid" name="Mid" type="hidden">
                </div>
                <div class="form-group">
                  <label for="Mestado">Estado</label>
                  <select class="form-control modal-input" id="Mestado" name="Mestado">
                    <option value="vigente">vigente</option>
                    <option value="terminado">terminado</option>
                    <option value="anulado">anulado</option>
                  </select>
                </div>
              </form>
              <!-- Fin formulario -->
            </div>
            <div class="modal-footer">
              <button id="confirmarMod" type="submit" class="btn btn-dark">Confirmar</button>
              <button type="button" class="btn btn-secondary" onclick="limpiarForm()" data-dismiss="modal">Cancelar</button>
            </div>
          </div>
        </div>
      </div>
      <!-- Fin Modal -->

      <!--Inicio modal de respuesta -->
      <div class="modal fade" id="Respuesta" role="dialog">
        <div class="modal-dialog" style="top:20%">
          <div class="modal-content" style="margin-bottom:0;">
            <div id="alertaR" class="modal-body" role="alert" style="position: relative; margin: 8px;">
              <button type="button" id="close-button" class="close" data-dismiss="modal">&times;</button>
              <div style="padding-top: 10px; margin: 0;">
                <p id="mensajeRespuesta"></p>
              </div> 
            </div>
          </div>
        </div>
      </div>
      <!-- Fin modal de respuesta -->

      <!--Inicio modal de confirmacion de Eliminar contrato-->
      <div class="modal fade" id="confirmar" role="dialog">
        <div class="modal-dialog" style="top:20%">
          <div class="modal-content" style="margin-bottom:0;">
            <div class="modal-header"><h5 class="modal-title" id="alertaModal">¿Seguro de eliminar el contrato seleccionado?</h5></div>
            <div id="alerta" class="modal-body" role="alert" style="position: relative; margin: 8px;">    
              <button type="button" onclick= "eliminarC()" id= "botoneliminar" class="btn btn-dark">Eliminar</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
          </div>
        </div>
      </div>
      <!-- Fin modal de confirmacion de Eliminar contrato -->

      <!-- Inicio modal de respuesta de Eliminar contrato -->
      <div class="modal fade" id="respuesta" role="dialog">
        <div class="modal-dialog" style="top:20%">
          <div class="modal-content" style="margin-bottom:0;">
            <div id="alertaE" class="modal-body" role="alert" style="position: relative; margin: 8px;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
              <div id="mensajeRes" class="form-group" style="padding-top: 10px; margin: 0;">
            </div>
            </div>
          </div>
        </div>
      </div>
      <!--- Fin modal de respuesta de Eliminar contrato-->
       
    </div>    
  </div>
  <!-- Pie de pagina -->
  <?php require_once '../includes/footer.php'?>
  <!-- Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.5.1.js" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

  <!-- Datatables JS -->
  <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.bootstrap4.min.js"></script>
  <script src="../../docs/js/datatableTrad.js"></script>
  <script src="../../docs/js/contratos.js"></script>

</body>
</html>