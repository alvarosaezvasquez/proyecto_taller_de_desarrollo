<?php
    namespace app\controllers;
    
    require_once $_SERVER["DOCUMENT_ROOT"].'/vendor/autoload.php';
    use app\models\Anexos;
    use app\models\Contratos;

    header('Content-Type: application/json');

    $anexos = new Anexos();
    
    $cantidadAnexo = $_POST['cantidad_anexo'];
    $idContrato = $_POST['idcontrato'];
    $cantidadOtrosAnexos= $anexos->sumaCantidadAnexos($idContrato);
    $contrato = new Contratos();
    $contratoID = $contrato->getContrato($idContrato);
    $cdate=0;
    $ahora = time();
    $cdate=$contratoID->fechainicio_contrato;
    
    $diasDiferencia = intval(abs(strtotime($cdate) - time()) /(3600*24));
    if (  $diasDiferencia > 90) {
        $respuesta = [
            "estado"=> -1, 
            "mensaje"=>"ERROR: la fecha de inicio del contrato supera los 90 dias"
        ];
        echo json_encode($respuesta);
    }
    else{
        if(!filter_input(INPUT_POST, 'cantidad_anexo', FILTER_VALIDATE_INT)){
            $respuesta = [
                "estado"=> -1, 
                "mensaje"=>"ERROR: campo cantidad de equipos debe ser un numero entero"
            ];
            echo json_encode($respuesta);
        }
        else if($cantidadAnexo < 1){
            $respuesta = [
                "estado"=> -1, 
                "mensaje"=>"ERROR: la cantidad debe ser un nuemro mayor a 1"
            ];
            echo json_encode($respuesta);
        }
        else{
            $cantidadContrato = intval($contratoID->cant_equipos);
            if(($cantidadContrato * 0.3 ) < ($cantidadAnexo + $cantidadOtrosAnexos)){
                $respuesta = [
                    "estado"=> -1, 
                    "mensaje"=>"ERROR: los anexos no pueden superar el 30% de la cantidad de equipos del contrato original"
                ];
                echo json_encode($respuesta);
            }
            else{
                //Nombre aleatorio para el documento subido
                $characters = '0123456789abcdefghijklmnopqrstuvwxyz'; 
                $randomString = ''; 
                for ($i = 0; $i<24; $i++) { 
                    $index = rand(0,strlen($characters) - 1); 
                    $randomString .= $characters[$index]; 
                }
                $target_dir = "../../archivos/";
                $target_file = $target_dir . $randomString . ".pdf";

                //Valida la extension y sube el pdf
                $docExt = strtolower(pathinfo(basename($_FILES["annexFile"] ["name"]),PATHINFO_EXTENSION));
                if ($docExt == 'pdf') {
                    move_uploaded_file($_FILES["annexFile"]["tmp_name"],$target_file);
                    $res = $anexos->crearAnexo($randomString.".pdf",$cantidadAnexo,$idContrato);
                    if ($res) {
                        $respuesta = [
                            "estado"=>0, 
                            "mensaje"=>"La información se ha registrado exitosamente"
                        ];
                        echo json_encode($respuesta);
                    }
                    else {
                        //Error de db
                        $respuesta = [
                            "estado"=> -1, 
                            "mensaje"=>"Error DB"
                        ];
                        echo json_encode($respuesta);
                    }
                } else {
                    //Error de formato
                    $respuesta = [
                        "estado"=> -1, 
                        "mensaje"=>"Error en el formato de archivo"
                    ];
                    echo json_encode($respuesta);
                }
            }
        }
    }
?>
