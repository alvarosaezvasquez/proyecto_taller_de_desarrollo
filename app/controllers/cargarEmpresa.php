<?php
    namespace app\controllers;
    require_once '../../vendor/autoload.php';
    use app\models\Empresas;
    use app\models\Dbh;

    header('Content-Type: application/json');

    $dato = filter_input(INPUT_POST,'id',FILTER_SANITIZE_STRING);

    $empresas = new Empresas();
    $empresa = $empresas->getEmpresa($dato);

    echo json_encode($empresa);

?>