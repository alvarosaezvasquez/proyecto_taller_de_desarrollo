<?php
    namespace app\controllers;
    require_once '../../vendor/autoload.php';
    use app\models\Empresas;
    header('Content-Type: application/json');

    $nombre_empresa = filter_input(INPUT_POST, 'nombreEmpresa', FILTER_SANITIZE_STRING);
    $rut_empresa = filter_input(INPUT_POST, 'rutEmpresa', FILTER_SANITIZE_STRING);
    $direccion_empresa = filter_input(INPUT_POST, 'dirEmpresa', FILTER_SANITIZE_STRING);
    $correo_empresa = filter_input(INPUT_POST, 'emailEmpresa', FILTER_SANITIZE_EMAIL);
    $telefono_empresa = filter_input(INPUT_POST, 'telefonoEmpresa', FILTER_SANITIZE_STRING);
    $sitioweb_empresa = filter_input(INPUT_POST, 'webEmpresa', FILTER_SANITIZE_URL);

    $errores = [];
    $count_err = 0;
    
    if(preg_match('/^[0-9a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/', $nombre_empresa) && $nombre_empresa !=""){
		array_push($errores, 0);
	}else{
		array_push($errores, 1);
        $count_err++;
	}

    if(preg_match('/^[0-9]+-[0-9kK]{1}/', $rut_empresa) && $rut_empresa !=""){
		array_push($errores, 0);
	}else{
		array_push($errores, 1);
        $count_err++;
	}

    if(preg_match('/^[0-9a-zA-ZáéíóúÁÉÍÓÚñÑ,\s]+$/', $direccion_empresa) && $direccion_empresa !=""){
		array_push($errores, 0);
	}else{
		array_push($errores, 1);
        $count_err++;
	}

    if(!filter_var($correo_empresa,FILTER_VALIDATE_EMAIL) || $correo_empresa == ""){
        array_push($errores, 1);
        $count_err++;
    }else{
        array_push($errores, 0);
    }

    if(!filter_var($telefono_empresa, FILTER_VALIDATE_INT) || $telefono_empresa == ""){
        array_push($errores, 1);
        $count_err ++;
    } else {
        array_push($errores, 0);
    }

    if($sitioweb_empresa == ""){
        array_push($errores,0);
    } else {
        if(!filter_var($sitioweb_empresa, FILTER_VALIDATE_URL)){
            array_push($errores, 1);
            $count_err ++;
        } else {
            array_push($errores, 0);
        }
    }

    if($telefono_empresa <= 0){
        array_push($errores, 1);
        $count_err ++;
    } else {
        array_push($errores, 0);
    }
    
    $existe = new Empresas();
    $existeRut = $existe->getEmpresaByRut($rut_empresa);

    if($existeRut == false){
        array_push($errores, 0);
    } else {
        array_push($errores, 1);
        $count_err ++;
    }

    $existeCorreo = $existe->getEmpresaByCorreo($correo_empresa);

    if($existeCorreo == false){
        array_push($errores, 0);
    } else {
        array_push($errores, 1);
        $count_err ++;
    }


    if($count_err==0){
        $nuevaEmpresa = new Empresas();
        $result = $nuevaEmpresa->postEmpresa($nombre_empresa, $rut_empresa, $correo_empresa, $telefono_empresa, $sitioweb_empresa, $direccion_empresa);
    } else {
        $result = false;
    }

    if($result === false){
        $respuesta = [
            "mensaje"=>"Error al registrar empresa",
            "errores"=>$errores,
            "conteo_err"=>$count_err
        ];
    } else {
        $respuesta = [
            "mensaje"=>"La empresa se ha registrado exitosamente",
            "errores"=>$errores,
            "conteo_err"=>$count_err
        ];
    }

    echo json_encode($respuesta);
    

    
?>