<?php
    namespace app\controllers;
    
    require_once '../../vendor/autoload.php';
    use app\models\Contratos;
    use app\models\Anexos;

    $target_dir = "../../archivos/";
    header('Content-Type: application/json');
        $dato = filter_input(INPUT_POST,'id',FILTER_SANITIZE_STRING);
        $contrato = new Contratos();
        $anexo = new Anexos();
        $idanexos = $anexo->getAnexosContrato($dato);
        
        if($idanexos != null){
            foreach ($idanexos as $tuplas){
                $archivo = $tuplas->nombre_anexo;
                $file_pointer = $target_dir . $archivo;
                unlink($file_pointer);
            }
        }
        $respuesta = $anexo->deleteContratoAnexo($dato);
        if($respuesta){
            $idcontrato = $contrato->getContrato($dato);
            $archivo = $idcontrato->nombre_contrato;
            $respuesta = $contrato->deleteContrato($dato);
            if($respuesta){
                $file_pointer = $target_dir . $archivo;
                unlink($file_pointer);
                $respuesta = [
                    "mensaje"=>"El contrato seleccionado se eliminó correctamente"
                ];
                echo json_encode($respuesta);
            }else {
                $respuesta = [
                    "mensaje"=>"No se logro eliminar el contrato"
                ];
                echo json_encode($respuesta);
            }
        }else{
            $respuesta = [
                "mensaje"=>"No se logro elimiar los anexos del contrato"
            ];
            echo json_encode($respuesta);
        }
?>