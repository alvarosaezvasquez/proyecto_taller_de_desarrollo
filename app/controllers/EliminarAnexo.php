<?php
    namespace app\controllers;
    
    require_once '../../vendor/autoload.php';
    use app\models\Anexos;

    $target_dir = "../../archivos/";
    header('Content-Type: application/json');
        $dato = filter_input(INPUT_POST, 'id' ,FILTER_SANITIZE_STRING);
        $anexo = new Anexos();
        $idcontrato = $anexo->getAnexo($dato);
        $archivo = $idcontrato->nombre_anexo;
        $respuesta = $anexo->deleteAnexo($dato);
        $file_pointer = $target_dir . $archivo;

        if($respuesta){
            unlink($file_pointer);
            $respuesta = ["mensaje"=> "El anexo  seleccionado se eliminó correctamente ",
            "contrato"=>$idcontrato->id_contrato];
            echo json_encode($respuesta);
        }else{
            $respuesta = ["mensaje"=>"No se logro eliminar el anexo","contrato"=>$idcontrato];
            echo json_encode($respuesta);
        }
    ?>