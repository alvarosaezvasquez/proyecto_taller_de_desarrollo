<?php
    namespace app\controllers;
    require_once '../../vendor/autoload.php';
    use app\models\Empresas;

    
    header('Content-Type: application/json');

    
    $id_empresa = $_POST['idMod'];
    $nombre_empresa = $_POST['nombreEmpresaMod'];
    $rut_empresa = $_POST['rutEmpresaMod'];
    $direccion_empresa = $_POST['dirEmpresaMod'];
    $correo_empresa = $_POST['emailEmpresaMod'];
    $telefono_empresa = $_POST['telefonoEmpresaMod'];
    $sitioweb_empresa = $_POST['webEmpresaMod'];
    $faltas = $_POST['faltasMod'];

    $errores = [];
    $count_err = 0;

    if(preg_match('/^[0-9a-zA-ZáéíóúÁÉÍÓÚñÑ\s]+$/', $nombre_empresa) && $nombre_empresa !=""){
		array_push($errores, 0);
	}else{
		array_push($errores, 1);
        $count_err++;
	}

    if(preg_match('/^[0-9]+-[0-9kK]{1}/', $rut_empresa) && $rut_empresa !=""){
		array_push($errores, 0);
	}else{
		array_push($errores, 1);
        $count_err++;
	}

    if(preg_match('/^[0-9a-zA-ZáéíóúÁÉÍÓÚñÑ,\s]+$/', $direccion_empresa) && $direccion_empresa !=""){
		array_push($errores, 0);
	}else{
		array_push($errores, 1);
        $count_err++;
	}

    if(!filter_var($correo_empresa,FILTER_VALIDATE_EMAIL) || $correo_empresa == ""){
        array_push($errores, 1);
        $count_err++;
    }else{
        array_push($errores, 0);
    }

    if(!filter_var($telefono_empresa, FILTER_VALIDATE_INT) || $telefono_empresa == ""){
        array_push($errores, 1);
        $count_err ++;
    } else {
        array_push($errores, 0);
    }

    if($sitioweb_empresa == ""){
        array_push($errores,0);
    } else {
        if(!filter_var($sitioweb_empresa, FILTER_VALIDATE_URL)){
            array_push($errores, 1);
            $count_err ++;
        } else {
            array_push($errores, 0);
        }
    }

    if($telefono_empresa <= 0){
        array_push($errores, 1);
        $count_err ++;
    } else {
        array_push($errores, 0);
    }

    if($faltas < 0){
        array_push($errores, 1);
        $count_err ++;
    } else {
        array_push($errores, 0);
    }

    if($count_err==0){
        $empresa = new Empresas();
        $result = $empresa->putEmpresa($id_empresa, $nombre_empresa, $rut_empresa, $correo_empresa, $telefono_empresa, $sitioweb_empresa,$faltas, $direccion_empresa);
    } else {
        $result = false;
    }
    

    if($result === false){
        $respuesta = [
            "mensaje"=>"Error al modificar empresa",
            "errores"=>$errores,
            "conteo_err"=>$count_err
        ];
    } else {
        $respuesta = [
            "mensaje"=>"La empresa se ha modificado exitosamente",
            "errores"=>$errores,
            "conteo_err"=>$count_err
        ];
        
    }

    echo json_encode($respuesta);

?>