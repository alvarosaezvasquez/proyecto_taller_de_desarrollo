<?php
    namespace app\controllers;
    
    require_once '../../vendor/autoload.php';
    use app\models\Contratos;
    use app\models\Empresas;

    header('Content-Type: application/json');

    $contrato = new Contratos();
    $empresa = new Empresas();
    $errores = [];

    //Sanitiza inputs
    $id_empr = filter_input(INPUT_POST, 'selEmpresa', FILTER_SANITIZE_STRING);
    $cant = filter_input(INPUT_POST, 'cantContrato', FILTER_SANITIZE_STRING);
    $date_i = filter_input(INPUT_POST, 'iniFecha', FILTER_SANITIZE_STRING);
    $date_f = filter_input(INPUT_POST, 'finFecha', FILTER_SANITIZE_STRING);

    //Verificacion de empresa seleccionada
    $empresa = $empresa->getEmpresa($id_empr);
    if (!$empresa) {
        array_push($errores, true);
    } else {
        array_push($errores, false);
    }

    //Verificacion de cantidad
    if (filter_var($cant, FILTER_VALIDATE_INT)){
        if ($cant < 1){
            array_push($errores, true);
        } else {
            array_push($errores, false);
        }
    } else {
        array_push($errores, true);
    }

    //Verificacion de las fechas
    if (strtotime($date_i) < strtotime($date_f)) {
        array_push($errores, false);
    } else {
        array_push($errores, true);
    }

    //Nombre aleatorio para el documento subido
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz'; 
    $randomString = ''; 
    for ($i = 0; $i<24; $i++) { 
        $index = rand(0,strlen($characters) - 1); 
        $randomString .= $characters[$index]; 
    }
    $target_dir = "../../archivos/";
    $target_file = $target_dir . $randomString . ".pdf";

    //Valida la extension del pdf
    $docExt = strtolower(pathinfo(basename($_FILES["archContrato"]["name"]),PATHINFO_EXTENSION));
    if ($docExt == 'pdf'){
        array_push($errores, false);
    } else {
        array_push($errores, true);
    }

    //Cuenta los errores
    $count = 0;
    foreach($errores as $value){
        if($value){
            $count++;
        }
    }

    //mueve el archivo y registra en bd
    if ($count==0) {
        move_uploaded_file($_FILES["archContrato"]["tmp_name"],$target_file);
        $res = $contrato->postContrato($id_empr,$randomString.".pdf",$date_i,$date_f,"vigente",$cant);
        if ($res) {
            $respuesta = [
                "mensaje"=>"La información se ha registrado exitosamente",
                "done"=>true,
                "nerr"=>0,
                "errores"=>$errores
            ];
            echo json_encode($respuesta);
        } else {
            $respuesta = [
                "mensaje"=>"Error al registrar la informacion",
                "done"=>false,
                "nerr"=>0,
                "errores"=>$errores
            ];
            echo json_encode($respuesta);
        }
    } else {
        $respuesta = [
            "mensaje"=>"Error en la informacion ingresada",
            "done"=>false,
            "nerr"=>$count,
            "errores"=>$errores
        ];
        echo json_encode($respuesta);
    }
?>