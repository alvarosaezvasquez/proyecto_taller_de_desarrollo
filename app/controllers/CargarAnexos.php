<?php
    namespace app\controllers;    
    require_once $_SERVER["DOCUMENT_ROOT"].'/vendor/autoload.php';
    use app\models\Anexos;   
if(isset( $_POST['dato'] )) {     
     $dato = filter_input(INPUT_POST,'dato',FILTER_SANITIZE_NUMBER_INT);
     $test = new Anexos();
     $result =$test->getAnexosContrato($dato) ;
     echo json_encode($result);
}
?>