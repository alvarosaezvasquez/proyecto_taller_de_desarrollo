<?php
    namespace app\controllers;
    
    require_once '../../vendor/autoload.php';
    use app\models\Contratos;

    header('Content-Type: application/json');
    $dato = filter_input(INPUT_POST,'id',FILTER_SANITIZE_STRING);

    $contrato = new Contratos();
    $contratos = $contrato->getContrato($dato);

    echo json_encode($contratos);
?>