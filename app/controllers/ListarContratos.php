<?php
    namespace app\controllers;
    require_once $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
    use app\models\Contratos;
    use app\models\Empresas;
    use app\views\AnexosView;

    $contrato = new Contratos();
    $contratos = $contrato->getContratos();

    $empresa = new Empresas();
    $empresas = $empresa->getEmpresas();

    $anexos = new AnexosView();
    $anexos->AnexosModal();

    include_once("../views/ContratosView.php");
?>