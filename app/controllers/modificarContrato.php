<?php
    namespace app\controllers;
    
    require_once '../../vendor/autoload.php';
    use app\models\Contratos;
    use app\models\Empresas;

    header('Content-Type: application/json');

    $contratos = new Contratos();
    $empresa = new Empresas();
    $errores = [];

    //Sanitiza inputs
    $id_c = filter_input(INPUT_POST, 'Mid', FILTER_SANITIZE_STRING);
    $id_empr = filter_input(INPUT_POST, 'MselEmpresa', FILTER_SANITIZE_STRING);
    $cant = filter_input(INPUT_POST, 'McantContrato', FILTER_SANITIZE_STRING);
    $date_i = filter_input(INPUT_POST, 'MiniFecha', FILTER_SANITIZE_STRING);
    $date_f = filter_input(INPUT_POST, 'MfinFecha', FILTER_SANITIZE_STRING);
    $estado = filter_input(INPUT_POST, 'Mestado', FILTER_SANITIZE_STRING);

    //Verificacion de empresa seleccionada
    $empresa = $empresa->getEmpresa($id_empr);
    if (!$empresa[0]) {
        array_push($errores, true);
    } else {
        array_push($errores, false);
    }

    //Verificacion de cantidad
    if (filter_var($cant, FILTER_VALIDATE_INT)){
        if ($cant < 1){
            array_push($errores, true);
        } else {
            array_push($errores, false);
        }
    } else {
        array_push($errores, true);
    }

    //Verificacion de las fechas
    if (strtotime($date_i) < strtotime($date_f)) {
        array_push($errores, false);
    } else {
        array_push($errores, true);
    }

    //Verificacion de id de contrato
    if (!$contratos->getContrato($id_c)){
        array_push($errores, true);
    } else {
        array_push($errores, false);
    }

    //Verificacion de estado de contrato
    if ($estado=='vigente' || $estado=='terminado' || $estado=='anulado'){
        array_push($errores, false);
    } else {
        array_push($errores, true);
    }

    //Obtiene el nombre actual del archivo de contrato
    $contrato = $contratos->getContrato($id_c);
    $oldFile = $contrato->nombre_contrato;
    $file_up = false;
    if (isset($_FILES['MarchContrato'])){
        $file_up = true;
        //Nombre aleatorio para el archivo subido
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz'; 
        $randomString = ''; 
        for ($i = 0; $i < 24; $i++) { 
            $index = rand(0,strlen($characters) - 1); 
            $randomString .= $characters[$index]; 
        }
        $target_dir = "../../archivos/";
        $target_file = $target_dir . $randomString . ".pdf";

        //Verifica la extension del archivo
        $docExt = strtolower(pathinfo(basename($_FILES["MarchContrato"]["name"]),PATHINFO_EXTENSION));
        if ($docExt=='pdf') {
            array_push($errores, false);
        } else {
            array_push($errores, false);
        }
    } else {
        array_push($errores, false);
    }

    //Cuenta los errores
    $count = 0;
    foreach($errores as $value){
        if($value){
            $count++;
        }
    }

    if ($count==0){
        if ($file_up){
            $res = $contratos->putContrato($id_c,$randomString.".pdf",$id_empr,$date_i,$date_f,$estado,$cant);
        } else {
            $res = $contratos->putContrato($id_c,$oldFile.".pdf",$id_empr,$date_i,$date_f,$estado,$cant);
        }
        if ($res) {
            $respuesta = [
                "mensaje"=>"La información se ha modificado exitosamente",
                "done"=>true,
                "nerr"=>0,
                "errores"=>$errores
            ];
        } else {
            $respuesta = [
                "mensaje"=>"Error al registrar la informacion",
                "done"=>false,
                "nerr"=>0,
                "errores"=>$errores
            ];
        }
        echo json_encode($respuesta);
    } else {
        $respuesta = [
            "mensaje"=>"Error en la informacion ingresada",
            "done"=>false,
            "nerr"=>$count,
            "errores"=>$errores
        ];
        echo json_encode($respuesta);
    }
?>