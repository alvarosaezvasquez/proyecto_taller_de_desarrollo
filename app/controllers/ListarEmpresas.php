<?php
    namespace app\controllers;
    require_once '../../vendor/autoload.php';
    use app\models\Empresas;

    $empresa = new Empresas();
    $empresas = $empresa->getEmpresas();

    include_once("../views/EmpresasView.php");
?>