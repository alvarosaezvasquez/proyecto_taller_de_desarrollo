
# Guía de Contribución

## Equipo de Desarrollo

### Autores
- [Giordano Salini](http://gitlabtrans.face.ubiobio.cl:8081/18430958) - Líder del Proyecto - giordano.salini1801@alumnos.ubiobio.cl
- [Álvaro Sáez](http://gitlabtrans.face.ubiobio.cl:8081/18822286) - Ingeniero de Base de Datos - alvaro.saez1801@alumnos.ubiobio.cl
- [German Alvarez](http://gitlabtrans.face.ubiobio.cl:8081/18037681) - Ingeniero de Desarrollo - german.alvarez1801@alumnos.ubiobio.cl
- [José Quilodrán](http://gitlabtrans.face.ubiobio.cl:8081/19108724) - Ingeniero de Desarrollo - jose.quilodran1801@alumnos.ubiobio.cl
-  [Matías Tardones](http://gitlabtrans.face.ubiobio.cl:8081/20022346) - Analista de Sistema - matias.tardones1801@alumnos.ubiobio.cl


## Desarrollo del código

### Arquitectura del Sistema

El sistema organiza el código en las siguientes carpetas:
- **app**: donde se guardan las clases de la aplicación en sí. Incluye las carpetas **controllers, includes, models y views**.
- **controllers**: responden a eventos en la vista e invoca al modelo para ver o modificar datos, actuando como intermediario entre ambos.
- **includes**: donde se almacenan trozos de página reutilizables, como lo es la cabecera.
- **models**: aquí se almacenan las clases que interactúan directamente con la base de datos.
- **views**: encargado de mostrar datos al usuario.
- **docs**: aquí se almacenan archivos necesarios para el funcionamiento de la página, como lo son los CSS, scripts en JavaScript e imágenes.
- **archivos**: en esta carpeta se guardan los archivos pdf que serán registrados en el sistema

### Base de Datos

Para conectar con la base de datos debes pedir las credenciales al **Lider del proyecto**, o en su defecto al **Ingeniero de Base de Datos**. Las credenciales deben almacenarse en el archivo *credenciales.json* que se encuentra en *app/models/*

### Namespaces para Autoload con el estándar PSR-4

- Todos los archivos de desarrollo **php** deben tener declarado su respectivo namespace, de acuerdo al directorio en que se encuentre la clase.

## Interacción con el repositorio

1. Crear una issue y una nueva rama a partir de **main**
    - Describir y listar las tareas que serán desarrolladas
    - Agregar la issue correspondiente
1. Desarrollar el código en la nueva rama creada
    - Realizar los commit's en la rama de la issue
    - Mencionar en el commit el numero de la issue
1. Esperar la aprobación por parte del [Maintainer's](http://gitlabtrans.face.ubiobio.cl:8081/18430958) de este repositorio para la integración de tú código a la rama **main**


### Archivos/Directorios que no deben ser versionados o enviados al repositorio

- `/archivos/*.pdf`
- `/app/models/credenciales.json`
- `vendor/*`

### Archivos/Directorios que no deben estar en ambientes de producción

- `Dockerfile/*`
- `composer.lock`
- `README.md`
- `CONTRIBUTING.md`
- `.gitignore`
- `.git/*`
- `sql/*`